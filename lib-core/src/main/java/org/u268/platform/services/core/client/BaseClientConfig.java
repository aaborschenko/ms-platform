package org.u268.platform.services.core.client;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

/**
 * Client configuration base
 *
 * @author Alexey Borschenko
 * @since 01-Apr-2020
 */
public class BaseClientConfig {

    @Bean
    public ErrorDecoder errorDecoder() {
        return new BackendErrorDecoder();
    }
}
