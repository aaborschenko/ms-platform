package org.u268.platform.services.core.client;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

/**
 * Basic {@link org.springframework.data.domain.Page} implementation to be used when Client reading Paged data from Server
 * <br/>
 * <b>NOTE: must be used in Feign Clients:</b>
 * <p>See: ie.realli.services.profile.client.ProfileMSProfileFeignClient#listProfiles</p>
 * <br/>
 * <code>
 *  @GetMapping("/profile/list/v2")
 *  ResponseEntity<RestPage<GetProfileResponseDTO>> listProfiles(
 *             @RequestHeader("Authorization") String authToken,
 *             Pageable pageable,
 *             @RequestParam(value = "role", required = false) ProfileRoles role) throws BackendException;
 * </code>
 *
 * @author Alexey Borschenko
 * @since 02-Apr-2020
 */
public class RestPage<T> extends PageImpl<T> {

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public RestPage(@JsonProperty("content") List<T> content,
                    @JsonProperty("number") int number,
                    @JsonProperty("size") int size,
                    @JsonProperty("totalElements") Long totalElements,
                    @JsonProperty("pageable") JsonNode pageable,
                    @JsonProperty("last") boolean last,
                    @JsonProperty("totalPages") int totalPages,
                    @JsonProperty("sort") JsonNode sort,
                    @JsonProperty("first") boolean first,
                    @JsonProperty("numberOfElements") int numberOfElements) {

        super(content, PageRequest.of(number, size), totalElements);
    }

    public RestPage(List<T> content, Pageable pageable, long total) {
        super(content, pageable, total);
    }

    public RestPage(List<T> content) {
        super(content);
    }

    public RestPage() {
        super(new ArrayList<>());
    }
}
