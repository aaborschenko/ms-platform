package org.u268.platform.services.core.security;

import java.util.List;

/**
 * User Profile data interface
 *
 * @author Alexey Borschenko
 * @since 02-Sep-2020
 */
public interface IProfile {

    String getPassword();

    UserID getUserID();

    List<String> getRoles();
}
