package org.u268.platform.services.core.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * Represents User Identifier: Email and Organization
 * <p>
 * Username should look like: organization/email
 *
 * @author Alexey Borschenko
 * @since 03-Sep-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserID {

    public static final String USER_ID_DELIMITER = "/";

    private String organization;
    private String email;

    /**
     * Creates new UserID from combined username: Organization / Email
     *
     * @param username
     * @return
     */
    public static UserID fromUsername(String username) {
        if (StringUtils.isEmpty(username)) {
            throw new IllegalArgumentException("username is empty");
        }

        String[] organizationAndEmailArray = username.split(USER_ID_DELIMITER);
        if (organizationAndEmailArray.length != 2) {
            throw new IllegalArgumentException("username must contain Organization and Email");
        }

        return new UserID(organizationAndEmailArray[0], organizationAndEmailArray[1]);
    }

    /**
     * Constructs combined username of Organization / Email
     *
     * @return
     */
    public String toUsername() {
        return organization + USER_ID_DELIMITER + email;
    }

}
