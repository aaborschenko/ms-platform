package org.u268.platform.services.core.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.u268.platform.services.core.dao.repository.IBaseRepository;
import org.u268.platform.services.core.dto.UuidDto;
import org.u268.platform.services.core.rest.exception.NotFoundException;

import java.util.UUID;

/**
 * Base class for CRUD services
 * <p>
 * E - target entity
 * <p>
 * C - CreateRequestDTO
 * <p>
 * U - UpdateRequestDTO
 * <p>
 * R - ResponseDTO
 *
 * @author Alexey Borschenko
 * @since 31-Aug-2020
 */
@Slf4j
public abstract class AbstractService<E extends UuidEntity, C, U extends UuidDto, R> {

    /**
     * Target entity class type
     */
    protected final Class<E> entityType;

    /**
     * Base repository of target entity
     */
    protected final IBaseRepository<E> baseRepository;

    /**
     * Base mapper
     */
    protected final BaseMapper<E, C, U, R> baseMapper;

    /**
     * Initializes service with class of target entity
     *
     * @param entityType
     * @param baseRepository
     * @param baseMapper
     */
    protected AbstractService(Class<E> entityType, IBaseRepository<E> baseRepository, BaseMapper<E, C, U, R> baseMapper) {
        this.entityType = entityType;
        this.baseRepository = baseRepository;
        this.baseMapper = baseMapper;
    }

    @Transactional
    public R create(C createRequestDto) {
        log.info("Creating new {}: {}", entityType.getName(), createRequestDto);
        final E newEntity = baseMapper.newEntity(createRequestDto);
        newEntity.setUuid(UUID.randomUUID().toString());
        onCreateBeforeSave(createRequestDto, newEntity);
        final R responseDto = baseMapper.entityToDto(baseRepository.save(newEntity));
        log.info("Created new {}}: {}", entityType.getName(), responseDto);
        return responseDto;
    }

    /**
     * This code will be executed right before saving entity
     *
     * @param createRequestDto   incoming dto
     * @param entityAfterMapping entity after Mapper done his job
     */
    protected abstract void onCreateBeforeSave(C createRequestDto, E entityAfterMapping);


    /**
     * Finds entity by UUID
     *
     * @param uuid
     * @return
     */
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public R read(final String uuid) {
        return baseMapper.entityToDto(ensure(uuid));
    }

    /**
     * Searches for entities using provided specification
     *
     * @param specification search specification
     * @param pageable      paging info
     * @return
     */
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Page<R> search(final Specification<E> specification, final Pageable pageable) {
        return baseRepository.findAll(specification, pageable).map(baseMapper::entityToDto);
    }

    /**
     * Updates target entity
     *
     * @param updatedRequestDto
     * @return
     */
    @Transactional
    public R update(U updatedRequestDto) {
        log.info("Updating {}, {}", entityType.getName(), updatedRequestDto);
        final E entityAfterMapping = baseMapper.updateEntity(updatedRequestDto, ensure(updatedRequestDto.getUuid()));
        return baseMapper.entityToDto(baseRepository.save(entityAfterMapping));
    }

    protected abstract void onUpdateBeforeSave(U updatedRequestDto, E entityAfterMapping);

    /**
     * Deletes entity by uuid
     *
     * @param uuid entity uuid
     * @return uuid of deleted entity
     */
    @Transactional
    public String delete(final String uuid) {
        log.debug("Deleting {} with uuid={}", entityType.getName(), uuid);
        final E entityToDelete = ensure(uuid);
        onDeleteBeforeDeleteFromRepo(entityToDelete);
        baseRepository.delete(entityToDelete);
        log.info("Deleted {} with uuid={}", entityType.getName(), uuid);
        return uuid;
    }

    /**
     * Executes service-specific code before calling repository.delete
     * <p>
     * Here can be some target entity dependencies cleanup code
     *
     * @param entityToDelete
     */
    protected abstract void onDeleteBeforeDeleteFromRepo(E entityToDelete);

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public E ensure(String uuid) {
        return baseRepository.findByUuid(uuid)
                .orElseThrow(() -> new NotFoundException("Can't find entity with uuid: " + uuid));

    }
}
