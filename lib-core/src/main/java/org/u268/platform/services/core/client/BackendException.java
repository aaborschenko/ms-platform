package org.u268.platform.services.core.client;

import lombok.Getter;
import org.u268.platform.services.core.dto.ApiErrorDTO;

/**
 * Common class for backend exceptions
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@Getter
public class BackendException extends Exception {

    private int status;
    private String details;

    public BackendException(ApiErrorDTO errorDTO) {
        super(errorDTO.getError());
        status = errorDTO.getStatus();
        details = errorDTO.getDetails();
    }

    public BackendException(int status, String message) {
        super(message);
        this.status = status;
    }
}
