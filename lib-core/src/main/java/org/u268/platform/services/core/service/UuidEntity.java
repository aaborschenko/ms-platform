package org.u268.platform.services.core.service;

/**
 * @author Alexey Borschenko
 * @since 05-Sep-2020
 */
public interface UuidEntity {

    public String getUuid();

    public void setUuid(String uuid);
}
