package org.u268.platform.services.core.security;

import org.springframework.security.core.AuthenticationException;

/**
 * @author Alexey Borschenko
 * @since 3-Sep-2020
 */
public class InvalidJwtAuthenticationException extends AuthenticationException {
    /**
     *
     */
    private static final long serialVersionUID = -761503632186596342L;

    public InvalidJwtAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidJwtAuthenticationException(String msg) {
        super(msg);
    }
}
