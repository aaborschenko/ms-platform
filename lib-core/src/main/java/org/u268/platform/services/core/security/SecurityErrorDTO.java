package org.u268.platform.services.core.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Security error
 *
 * @author Alexey Borschenko
 * @since 3-Sep-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SecurityErrorDTO {
    private int status;
    private String message;
}
