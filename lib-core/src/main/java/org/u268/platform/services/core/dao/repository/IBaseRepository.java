package org.u268.platform.services.core.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
 * Base repository
 *
 * @author Alexey Borschenko
 * @since 31-Aug-2020
 */
public interface IBaseRepository<E> extends JpaRepository<E, Long>, JpaSpecificationExecutor<E> {

    Optional<E> findByUuid(String uuid);
}
