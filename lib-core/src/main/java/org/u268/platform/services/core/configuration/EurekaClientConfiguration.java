package org.u268.platform.services.core.configuration;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

/**
 * Eureka Client Configuration
 */
@Configuration
@EnableDiscoveryClient
public class EurekaClientConfiguration {
}
