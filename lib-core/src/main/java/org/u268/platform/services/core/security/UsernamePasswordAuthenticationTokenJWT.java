package org.u268.platform.services.core.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Custom Authentication Token implementation able to hold JWT
 *
 * @author Alexey Borschenko
 * @since 3-Sep-2020
 */
public class UsernamePasswordAuthenticationTokenJWT extends UsernamePasswordAuthenticationToken {

    private String jwt;

    public UsernamePasswordAuthenticationTokenJWT(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String jwt) {
        super(principal, credentials, authorities);
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }
}
