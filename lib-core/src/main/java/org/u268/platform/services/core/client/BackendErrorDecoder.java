package org.u268.platform.services.core.client;

import com.google.gson.Gson;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.extern.slf4j.Slf4j;
import org.u268.platform.services.core.dto.ApiErrorDTO;

/**
 * Error decoder for Feign clients
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@Slf4j
public class BackendErrorDecoder implements ErrorDecoder {

    @Override
    public BackendException decode(String methodKey, Response response) {
        final String responseBody = response.body().toString();
        log.info("decode: methodKey={}, response.status={}, response.body={}",
                methodKey, response.status(), responseBody);
        try {
            return new BackendException(new Gson().fromJson(responseBody, ApiErrorDTO.class));
        } catch (Exception e) {
            log.warn("Unable translate backend error JSON into ApiErrorDTO.class: " + e.getMessage(), e);
        }

        return new BackendException(response.status(), responseBody);
    }
}
