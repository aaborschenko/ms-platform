package org.u268.platform.services.core.service;

/**
 * Base data mapper with generics to apply for {@link AbstractService}
 *
 * @author Alexey Borschenko
 * @since Sep-5-2020
 */
public interface BaseMapper<E, C, U, R> {

    public R entityToDto(E entity);

    public E newEntity(C dto);

    public E updateEntity(U dto, E entityToUpdate);
}
