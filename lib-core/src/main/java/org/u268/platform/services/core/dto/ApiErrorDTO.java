package org.u268.platform.services.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.Serializable;

/**
 * @author Alexey Borschenko
 * @since 19-Sep-2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiErrorDTO implements Serializable {
    private int status;
    private String error;
    private String details;

    public ApiErrorDTO(Throwable t) {
        status = 500;
        error = t.getMessage();
        details = ExceptionUtils.getStackTrace(t);
    }
}
