package org.u268.platform.services.core.rest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Alexey Borschenko
 * @since 03-Jan-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "Service Version response DTO")
public class VersionResponseDTO implements Serializable {

    @ApiModelProperty(notes = "Branch Name")
    private String branchName;

    @ApiModelProperty(notes = "Service Build Time")
    private String buildTime;

    @ApiModelProperty(notes = "Service Build Version (maven version)")
    private String buildVersion;

    @ApiModelProperty(notes = "GIT Commit ID")
    private String commitId;

    @ApiModelProperty(notes = "GIT Commit time")
    private String commitTime;
}
