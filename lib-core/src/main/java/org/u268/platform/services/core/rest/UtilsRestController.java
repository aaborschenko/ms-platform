package org.u268.platform.services.core.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Holds common utility endpoints used by all REST services
 *
 * @author Alexey Borschenko
 * @since 13-Aug-2020
 */
@Slf4j
@RestController
@RequestMapping("/utils")
@Api(value = "Utils Controller")
public class UtilsRestController {

    @Value("${git.branch:bname}")
    private String branchName;

    @Value("${git.commit.id:-1}")
    private String commitId;

    @Value("${git.build.time:2020/10/4}")
    private String buildTime;

    @Value("${git.build.version:0.0.0}")
    private String buildVersion;

    @Value("${git.commit.time:2020/10/3}")
    private String commitTime;

    /**
     * Reads current service build info
     *
     * @return
     */
    @ApiOperation(value = "Read current build version info", response = VersionResponseDTO.class)
    @GetMapping(path = "/version/v1", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<VersionResponseDTO> getVersionInfo() {
        log.debug("Started UtilsRestController.getVersionInfo");
        final VersionResponseDTO dto = new VersionResponseDTO(branchName, buildTime, buildVersion, commitId, commitTime);
        log.debug("Version response: " + dto);
        return ResponseEntity.ok(dto);
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        PropertySourcesPlaceholderConfigurer propsConfig
                = new PropertySourcesPlaceholderConfigurer();
        propsConfig.setLocation(new ClassPathResource("version.properties"));
        propsConfig.setIgnoreResourceNotFound(true);
        propsConfig.setIgnoreUnresolvablePlaceholders(true);
        return propsConfig;
    }

}
