package org.u268.platform.services.core.dto;

/**
 * @author Alexey Borschenko
 * @since 05-Sep-2020
 */
public interface UuidDto {

    public String getUuid();

    public void setUuid(String uuid);
}
