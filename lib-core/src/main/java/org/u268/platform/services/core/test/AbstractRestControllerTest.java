package org.u268.platform.services.core.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.u268.platform.services.core.util.JsonUtils.getObjectMapper;

/**
 * Base class for REST Controller Tests
 *
 * @author Alexey Borschenko
 * @since 31-Aug-2020
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public abstract class AbstractRestControllerTest extends Assert {

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected Gson gson;

    protected <T> String mapToJson(final T obj) throws JsonProcessingException {
        return prepareMapper().writeValueAsString(obj);
    }

    protected <T> T mapFromJson(final String obj, Class<T> clazz) throws JsonProcessingException {
        return prepareMapper().readValue(obj, clazz);
    }

    private ObjectMapper prepareMapper() {
        return getObjectMapper()
                .registerModule(new JavaTimeModule());
    }
}
