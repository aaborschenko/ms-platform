package org.u268.platform.services.core.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger UI
 *
 * @author Alexey Borschenko
 * @since 01-Sep-2020
 */
@Configuration
@EnableSwagger2
@Profile("!production") // disabling SwaggerUI for *production* profile
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.u268.platform"))
                .paths(PathSelectors.any())
                .build();
    }
}
