package org.u268.platform.services.core.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.StringJoiner;

import static java.util.stream.Collectors.toList;

/**
 * User Details implementation
 *
 * @author Alexey Borschenko
 * @since 03-Sep-2020
 */
public class UserProfileDetails implements UserDetails {

    private final String password;
    private final String username;
    private List<String> roles = new ArrayList<>();

    public UserProfileDetails(IProfile profile) {
        this.password = profile.getPassword();
        this.username = profile.getUserID().toUsername();
        this.roles = profile.getRoles();
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles.stream().map(SimpleGrantedAuthority::new).collect(toList());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", UserProfileDetails.class.getSimpleName() + "[", "]")
                .add("roles=" + roles)
                .add("username='" + username + "'")
                .toString();
    }
}
