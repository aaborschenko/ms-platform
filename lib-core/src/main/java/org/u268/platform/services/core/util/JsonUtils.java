package org.u268.platform.services.core.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Alexey Borschenko
 * @since 31-Aug-2020
 */
public interface JsonUtils {

    static ObjectMapper getObjectMapper() {
        return new ObjectMapper(); // TODO configure mapper
    }

    static <T> String mapToJson(final T obj) throws JsonProcessingException {
        return getObjectMapper().writeValueAsString(obj);
    }

    static <T> T mapFromJson(final String json, final Class<T> clazz) throws JsonProcessingException {
        return getObjectMapper().readValue(json, clazz);
    }
}
