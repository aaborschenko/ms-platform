package org.u268.platform.services.core.test;

import org.testcontainers.containers.PostgreSQLContainer;

/**
 * Test container for U268 Platform Postgresql DB
 *
 * @author Alexey Borschenko
 * @since 31-Aug-2020
 */
public class PlatformPostgresqlTestContainer extends PostgreSQLContainer<PlatformPostgresqlTestContainer> {
    private static final String IMAGE_VERSION = "postgres:11.1";
    private static PlatformPostgresqlTestContainer container;

    private PlatformPostgresqlTestContainer() {
        super(IMAGE_VERSION);
    }

    public static PlatformPostgresqlTestContainer getInstance() {
        if (container == null) {
            container = new PlatformPostgresqlTestContainer();
        }
        return container;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}