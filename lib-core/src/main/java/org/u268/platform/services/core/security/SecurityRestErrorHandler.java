package org.u268.platform.services.core.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.ResponseEntity.status;

/**
 * @author Alexey Borschenko
 * @since 3-Sep-2020
 */
@RestControllerAdvice
@Slf4j
public class SecurityRestErrorHandler {

    @ExceptionHandler(value = {InvalidJwtAuthenticationException.class})
    @SuppressWarnings("rawtypes")
    public ResponseEntity invalidJwtAuthentication(InvalidJwtAuthenticationException ex, WebRequest request) {
        log.trace("handling InvalidJwtAuthenticationException...");
        return status(UNAUTHORIZED).build();
    }
}
