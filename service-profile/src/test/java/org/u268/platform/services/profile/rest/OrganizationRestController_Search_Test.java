package org.u268.platform.services.profile.rest;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.u268.platform.services.core.test.AbstractRestControllerWithPostgresqlTest;
import org.u268.platform.services.profile.ProfileServiceApplication;
import org.u268.platform.services.profile.dto.OrganizationCreateRequestDTO;
import org.u268.platform.services.profile.dto.OrganizationResponseDTO;
import org.u268.platform.services.profile.dto.OrganizationUpdateRequestDTO;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for search implementation at {@link org.u268.platform.services.profile.rest.OrganizationRestController}
 *
 * @author Alexey Borschenko
 * @since 14-Sep-2020
 */
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = ProfileServiceApplication.class)
public class OrganizationRestController_Search_Test extends AbstractRestControllerWithPostgresqlTest {

    @Test
    public void test01_read() throws Exception {

        String responseAsString = mvc.perform(get("/organizations/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(3))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test01_read responseAsString = " + responseAsString);
    }

    @Test
    public void test02_read() throws Exception {

        String responseAsString = mvc.perform(get("/organizations/v1?nameStart=Noname")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test02_read responseAsString = " + responseAsString);
    }

    @Test
    public void test03_read() throws Exception {

        String responseAsString = mvc.perform(get("/organizations/v1?nameStart=Fir")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c17e"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test03_read 1 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/organizations/v1?nameStart=Sec")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test03_read 2 responseAsString = " + responseAsString);
    }
}
