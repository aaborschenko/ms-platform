package org.u268.platform.services.profile.rest;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.u268.platform.services.core.test.AbstractRestControllerWithPostgresqlTest;
import org.u268.platform.services.profile.ProfileServiceApplication;
import org.u268.platform.services.profile.dto.AuthRequestDTO;
import org.u268.platform.services.profile.dto.AuthResponseDTO;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for Auth
 *
 * @author Alexey Borschenko
 * @since 24-Sep-2020
 */
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = ProfileServiceApplication.class)
public class AuthRestControllerTest extends AbstractRestControllerWithPostgresqlTest {

    public static final String ENCODED_PASSWORD = "$2a$10$ukSpo.Ryms/MOgxnjOxriepSjyeM1e0L/SG7A4KQojI0.QesvRc9K";

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void test_encoder() {
        assertTrue(passwordEncoder.matches(
                "password",
                ENCODED_PASSWORD));
    }

    @Test
    public void test01_login_fail() throws Exception {

        AuthRequestDTO requestDTO = new AuthRequestDTO(
                "b2c7acce-7abb-49ef-91d7-556e0e19c17e",
                "profile1@test",
                "wrong_pass"
        );

        String responseAsString = mvc.perform(post("/auth/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(requestDTO)))
                // Then
                .andExpect(status().is4xxClientError())
                .andExpect(status().is(403))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test01_login_fail responseAsString = " + responseAsString);
    }

    @Test
    public void test01_login_ok_readInfo() throws Exception {

        AuthRequestDTO requestDTO = new AuthRequestDTO(
                "b2c7acce-7abb-49ef-91d7-556e0e19c17e",
                "profile1@test",
                "password"
        );

        String responseAsString = mvc.perform(post("/auth/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(requestDTO)))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.organizationUuid").value(requestDTO.getOrganizationUuid()))
                .andExpect(jsonPath("$.email").value(requestDTO.getEmail()))
                .andExpect(jsonPath("$.uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andExpect(jsonPath("$.roles").exists())
                .andExpect(jsonPath("$.roles").isArray())
                .andExpect(jsonPath("$.roles.length()").value(1))
                .andExpect(jsonPath("$.roles[0]").value("ROLE_USER"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test01_login_ok responseAsString = " + responseAsString);

        AuthResponseDTO responseDTO = mapFromJson(responseAsString, AuthResponseDTO.class);
        assertNotNull(responseDTO.getToken());

        responseAsString = mvc.perform(get("/auth/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header("Authorization", "Bearer " + responseDTO.getToken()))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.organizationUuid").value(requestDTO.getOrganizationUuid()))
                .andExpect(jsonPath("$.email").value(requestDTO.getEmail()))
                .andExpect(jsonPath("$.uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andExpect(jsonPath("$.token").value(responseDTO.getToken()))
                .andExpect(jsonPath("$.roles").exists())
                .andExpect(jsonPath("$.roles").isArray())
                .andExpect(jsonPath("$.roles.length()").value(1))
                .andExpect(jsonPath("$.roles[0]").value("ROLE_USER"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test01_login_ok responseAsString = " + responseAsString);
    }
}
