package org.u268.platform.services.profile.client;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;
import org.u268.platform.services.core.security.UserProfileDetails;
import org.u268.platform.services.core.security.UsernamePasswordAuthenticationTokenJWT;
import org.u268.platform.services.core.test.AbstractRestControllerWithPostgresqlTest;
import org.u268.platform.services.profile.dao.entity.Profile;
import org.u268.platform.services.profile.dao.repository.ProfileRepository;
import org.u268.platform.services.profile.dto.AuthResponseDTO;

/**
 * Tests for service client: {@link ServiceProfileClient}
 *
 * @author Alexey Borschenko
 * @since 24-Sep-2020
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
//@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ServiceProfileClientTest extends AbstractRestControllerWithPostgresqlTest {

    @Autowired
    private ServiceProfileClient client;

    @Autowired
    private ProfileRepository profileRepository;

    @Test
    public void test_login_readInfo() {
        AuthResponseDTO responseDTO = client.login(
                "b2c7acce-7abb-49ef-91d7-556e0e19c17e",
                "profile1@test",
                "password");

        System.out.println("responseDTO = " + responseDTO);

        assertNotNull(responseDTO.getToken());
        assertEquals("profile1@test", responseDTO.getEmail());
        assertEquals("b2c7acce-7abb-49ef-91d7-556e0e19c17e", responseDTO.getOrganizationUuid());

        loginUser(responseDTO.getOrganizationUuid(), responseDTO.getEmail(), responseDTO.getToken());

        AuthResponseDTO readResponse = client.getLoginInfo();
        assertNotNull(readResponse);
    }

    @Transactional(readOnly = true)
    public void loginUser(String organizationUuid, String email, String jwt) {
        final Profile profile = profileRepository.findByEmailAndOrganizationUuid(email, organizationUuid).get();
        final UserDetails userDetails = new UserProfileDetails(profile);
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationTokenJWT(userDetails, "",
                        userDetails.getAuthorities(), jwt));
    }
}
