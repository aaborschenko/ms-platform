package org.u268.platform.services.profile.rest;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.u268.platform.services.core.test.AbstractRestControllerWithPostgresqlTest;
import org.u268.platform.services.profile.ProfileServiceApplication;
import org.u268.platform.services.profile.dto.ProfileCreateRequestDTO;
import org.u268.platform.services.profile.dto.ProfileResponseDTO;
import org.u268.platform.services.profile.dto.ProfileUpdateRequestDTO;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for {@link org.u268.platform.services.profile.rest.ProfileRestController}
 *
 * @author Alexey Borschenko
 * @since 5-Sep-2020
 */
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = ProfileServiceApplication.class)
public class ProfileRestControllerTest extends AbstractRestControllerWithPostgresqlTest {

    @Test
    public void test01_create_update() throws Exception {
        ProfileCreateRequestDTO createRequestDTO = new ProfileCreateRequestDTO(
                "profiletest01@test", true, "password", 
                "profiletest01_secondary@test", "Test1FN", "Test1LN", "123123123123",
                "b2c7acce-7abb-49ef-91d7-556e0e19c17e"
        );

        String responseAsString = mvc.perform(post("/profiles/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(createRequestDTO)))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value(createRequestDTO.getEmail()))
                .andExpect(jsonPath("$.enabled").value(createRequestDTO.getEnabled()))
                .andExpect(jsonPath("$.secondaryEmail").value(createRequestDTO.getSecondaryEmail()))
                .andExpect(jsonPath("$.firstName").value(createRequestDTO.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(createRequestDTO.getLastName()))
                .andExpect(jsonPath("$.phone").value(createRequestDTO.getPhone()))
                .andExpect(jsonPath("$.organizationUuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c17e"))
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andExpect(jsonPath("$.password").doesNotExist())
                .andReturn().getResponse().getContentAsString();

        System.out.println("ProfileRestControllerTest.test01_create_update responseAsString = " + responseAsString);

        ProfileResponseDTO test01_created = mapFromJson(responseAsString, ProfileResponseDTO.class);
        System.out.println("ProfileRestControllerTest.test01_created = " + test01_created);

        assertNotNull("Next step requires data from Create call", test01_created);

        //update previously created record
        ProfileUpdateRequestDTO updateRequestDTO = new ProfileUpdateRequestDTO(
                test01_created.getUuid(),
                test01_created.getSecondaryEmail() + ".com",
                "UpdatedFN", "UpdatedLN", null
        );

        String updateResponseAsString = mvc.perform(put("/profiles/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(updateRequestDTO)))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value(createRequestDTO.getEmail()))
                .andExpect(jsonPath("$.firstName").value(updateRequestDTO.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(updateRequestDTO.getLastName()))
                .andExpect(jsonPath("$.phone").value(createRequestDTO.getPhone())) //phone should remain unchanged here
                .andExpect(jsonPath("$.uuid").value(updateRequestDTO.getUuid()))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andReturn().getResponse().getContentAsString();

        System.out.println("test01_create_update updateResponseAsString = " + responseAsString);
    }

    @Test
    public void test02_read() throws Exception {

        String responseAsString = mvc.perform(get("/profiles/v1/b2c7acce-7abb-49ef-91d7-556e0e19c18e")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value("profile1@test"))
                .andExpect(jsonPath("$.uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andExpect(jsonPath("$.secondaryEmail").value("profile1_secondary@test"))
                .andExpect(jsonPath("$.enabled").value(true))
                .andExpect(jsonPath("$.firstName").value("John"))
                .andExpect(jsonPath("$.lastName").value("Doe"))
                .andExpect(jsonPath("$.phone").value("1234567890"))
                .andExpect(jsonPath("$.organizationUuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c17e"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andReturn().getResponse().getContentAsString();

        System.out.println("ProfileRestControllerTest.test02_read responseAsString = " + responseAsString);
    }

    @Test
    public void test03_update() throws Exception {

        //update existing DB record - inserted as test data
        ProfileUpdateRequestDTO dto = new ProfileUpdateRequestDTO(
                "b2c7acce-7abb-49ef-91d7-556e0e19c18e",
                "upd_2nd@email.com",
                "UpdatedFN_", "UpdatedLN_", "123123123333"
        );

        String responseAsString = mvc.perform(put("/profiles/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(dto)))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value("profile1@test"))
                .andExpect(jsonPath("$.uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andExpect(jsonPath("$.secondaryEmail").value(dto.getSecondaryEmail()))
                .andExpect(jsonPath("$.enabled").value(true))
                .andExpect(jsonPath("$.firstName").value(dto.getFirstName()))
                .andExpect(jsonPath("$.lastName").value(dto.getLastName()))
                .andExpect(jsonPath("$.phone").value(dto.getPhone()))
                .andExpect(jsonPath("$.organizationUuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c17e"))
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andReturn().getResponse().getContentAsString();

        System.out.println("test03_create responseAsString = " + responseAsString);
    }
}
