package org.u268.platform.services.profile.rest;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.u268.platform.services.core.test.AbstractRestControllerWithPostgresqlTest;
import org.u268.platform.services.profile.ProfileServiceApplication;
import org.u268.platform.services.profile.dto.OrganizationCreateRequestDTO;
import org.u268.platform.services.profile.dto.OrganizationResponseDTO;
import org.u268.platform.services.profile.dto.OrganizationUpdateRequestDTO;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for {@link org.u268.platform.services.profile.rest.OrganizationRestController}
 *
 * @author Alexey Borschenko
 * @since 31-Aug-2020
 */
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = ProfileServiceApplication.class)
public class OrganizationRestControllerTest extends AbstractRestControllerWithPostgresqlTest {

    @Test
    public void test01_create_update() throws Exception {
        OrganizationCreateRequestDTO dto = new OrganizationCreateRequestDTO(
                "Test Organization #1",
                "Test Organization Description: The organization overview in a nonprofit job " +
                        "description shares key descriptors of the organization. It should include " +
                        "information that will help interested external candidates better assess their " +
                        "fit with the organization and better understand the organization's goals and " +
                        "beneficiaries."
        );

        String responseAsString = mvc.perform(post("/organizations/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(dto)))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(dto.getName()))
                .andExpect(jsonPath("$.description").value(dto.getDescription()))
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andReturn().getResponse().getContentAsString();

        System.out.println("test01_create_update responseAsString = " + responseAsString);

        OrganizationResponseDTO test01_created = mapFromJson(responseAsString, OrganizationResponseDTO.class);
        System.out.println("test01_created = " + test01_created);

        assertNotNull("Next step requires data from Create call", test01_created);

        //update previously created record
        OrganizationUpdateRequestDTO updateRequestDTO = new OrganizationUpdateRequestDTO(
                test01_created.getUuid(),
                test01_created.getName() + " UPDATED",
                null
        );

        String updateResponseAsString = mvc.perform(put("/organizations/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(updateRequestDTO)))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(updateRequestDTO.getName()))
                .andExpect(jsonPath("$.description").isNotEmpty())
                .andExpect(jsonPath("$.description").value(test01_created.getDescription()))
                .andExpect(jsonPath("$.uuid").value(updateRequestDTO.getUuid()))
                .andExpect(jsonPath("$.id").doesNotExist())
                .andReturn().getResponse().getContentAsString();

        System.out.println("test01_create_update updateResponseAsString = " + responseAsString);
    }

    @Test
    public void test02_read() throws Exception {

        String responseAsString = mvc.perform(get("/organizations/v1/b2c7acce-7abb-49ef-91d7-556e0e19c17e")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("First Organization"))
                .andExpect(jsonPath("$.description").value("Test Organization #1 Looooong Description"))
                .andExpect(jsonPath("$.uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c17e"))
                .andExpect(jsonPath("$.id").doesNotExist())
                .andReturn().getResponse().getContentAsString();

        System.out.println("test02_read responseAsString = " + responseAsString);
    }

    @Test
    public void test03_update() throws Exception {

        //update existing DB record - inserted as test data
        OrganizationUpdateRequestDTO dto = new OrganizationUpdateRequestDTO(
                "b2c7acce-7abb-49ef-91d7-556e0e19c17e",
                "Test Organization Updated",
                "Test Organization Description: The organization overview in a nonprofit job " +
                        "description shares key descriptors of the organization. It should include " +
                        "information that will help interested external candidates better assess their " +
                        "fit with the organization and better understand the organization's goals and " +
                        "beneficiaries."
        );

        String responseAsString = mvc.perform(put("/organizations/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(mapToJson(dto)))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(dto.getName()))
                .andExpect(jsonPath("$.description").value(dto.getDescription()))
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andReturn().getResponse().getContentAsString();

        System.out.println("test03_create responseAsString = " + responseAsString);
    }
}
