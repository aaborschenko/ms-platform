package org.u268.platform.services.profile.rest;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.u268.platform.services.core.test.AbstractRestControllerWithPostgresqlTest;
import org.u268.platform.services.profile.ProfileServiceApplication;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Tests for search implementation at {@link org.u268.platform.services.profile.rest.ProfileRestController}
 *
 * @author Alexey Borschenko
 * @since 15-Sep-2020
 */
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = ProfileServiceApplication.class)
public class ProfileRestController_Search_Test extends AbstractRestControllerWithPostgresqlTest {

    @Test
    public void test01() throws Exception {

        String responseAsString = mvc.perform(get("/profiles/v1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(4))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test01 responseAsString = " + responseAsString);
    }

    @Test
    public void test02_emailStart() throws Exception {

        String responseAsString = mvc.perform(get("/profiles/v1?emailStart=unknown")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test02 1 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?emailStart=prof")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(3))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test02 2 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?emailStart=profile1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test02 2 responseAsString = " + responseAsString);
    }

    @Test
    public void test03_firstNameStart() throws Exception {

        String responseAsString = mvc.perform(get("/profiles/v1?firstNameStart=unknown")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test03_firstNameStart 1 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?firstNameStart=Jo")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test03_firstNameStart 2 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?firstNameStart=Jo&emailStart=prof")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test03_firstNameStart 3 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?firstNameStart=Jo&emailStart=profile2")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test03_firstNameStart 4 responseAsString = " + responseAsString);
    }

    @Test
    public void test04_lastNameStart() throws Exception {

        String responseAsString = mvc.perform(get("/profiles/v1?lastNameStart=unknown")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test04_lastNameStart 1 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?lastNameStart=D")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(3))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test04_lastNameStart 2 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?lastNameStart=Do")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andExpect(jsonPath("$.content[0].lastName").value("Doe"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test04_lastNameStart 3 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?lastNameStart=Da")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c21e"))
                .andExpect(jsonPath("$.content[0].lastName").value("Damon"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test04_lastNameStart 4 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?lastNameStart=D&emailStart=prof")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(2))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test04_lastNameStart 5 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?lastNameStart=Do&emailStart=profile2")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test04_lastNameStart 6 responseAsString = " + responseAsString);
    }

    @Test
    public void test05_phoneStart() throws Exception {

        String responseAsString = mvc.perform(get("/profiles/v1?phoneStart=unknown")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test05_phoneStart 1 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?phoneStart=1")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(2))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c15a"))
                .andExpect(jsonPath("$.content[1].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c18e"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test05_phoneStart 2 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?phoneStart=23&emailStart=prof")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].uuid").value("b2c7acce-7abb-49ef-91d7-556e0e19c22e"))
                .andExpect(jsonPath("$.content[0].phone").value("234234234"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test05_phoneStart 3 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?phoneStart=24&emailStart=profile2")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test05_phoneStart 4 responseAsString = " + responseAsString);
    }

    @Test
    public void test06_organizationUuid() throws Exception {

        String responseAsString = mvc.perform(get("/profiles/v1?organizationUuid=unknown")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(0))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test06_organizationUuid 1 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?sort=firstName,asc&organizationUuid=b2c7acce-7abb-49ef-91d7-556e0e19c17e")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(2))
                .andExpect(jsonPath("$.content[0].firstName").value("John"))
                .andExpect(jsonPath("$.content[1].firstName").value("Matt"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test06_organizationUuid 2 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?sort=firstName,desc&organizationUuid=b2c7acce-7abb-49ef-91d7-556e0e19c17e")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(2))
                .andExpect(jsonPath("$.content[0].firstName").value("Matt"))
                .andExpect(jsonPath("$.content[1].firstName").value("John"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test06_organizationUuid 3 responseAsString = " + responseAsString);

        responseAsString = mvc.perform(get("/profiles/v1?organizationUuid=b2c7acce-7abb-49ef-91d7-556e0e19c18e")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content.length()").value(1))
                .andExpect(jsonPath("$.content[0].firstName").value("Luke"))
                .andReturn().getResponse().getContentAsString();

        System.out.println("test06_organizationUuid 4 responseAsString = " + responseAsString);
    }
}
