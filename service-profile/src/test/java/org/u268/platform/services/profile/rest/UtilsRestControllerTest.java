package org.u268.platform.services.profile.rest;

import org.junit.Test;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.u268.platform.services.core.test.AbstractRestControllerTest;
import org.u268.platform.services.core.test.AbstractRestControllerWithPostgresqlTest;
import org.u268.platform.services.profile.ProfileServiceApplication;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Integration tests for {@link org.u268.platform.services.core.rest.UtilsRestController}
 *
 * @author Alexey Borschenko
 * @since 31-Aug-2020
 */
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@SpringBootTest(classes = ProfileServiceApplication.class)
public class UtilsRestControllerTest extends AbstractRestControllerWithPostgresqlTest {

    @Test
    public void shouldBeAbleToReadVersion() throws Exception {
        // When
        mvc.perform(get("/utils/version/v1"))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.branchName").isNotEmpty())
                .andExpect(jsonPath("$.commitId").isNotEmpty())
                .andExpect(jsonPath("$.buildTime").isNotEmpty())
                .andExpect(jsonPath("$.buildVersion").isNotEmpty())
                .andExpect(jsonPath("$.commitTime").isNotEmpty());

    }
}