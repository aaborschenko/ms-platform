insert into organization(id, uuid, name, description)
VALUES (1, 'b2c7acce-7abb-49ef-91d7-556e0e19c17e', 'First Organization', 'Test Organization #1 Looooong Description'),
       (2, 'b2c7acce-7abb-49ef-91d7-556e0e19c18e', 'Second Organization', 'Test Organization #2 Looooong Description');
ALTER SEQUENCE organization_id_seq RESTART WITH 1000;

insert into profile(id, email, password, uuid, secondary_email, first_name, last_name, phone, enabled, organization_id)
VALUES (1, 'profile1@test', '$2a$10$ukSpo.Ryms/MOgxnjOxriepSjyeM1e0L/SG7A4KQojI0.QesvRc9K', 'b2c7acce-7abb-49ef-91d7-556e0e19c18e', 'profile1_secondary@test',
        'John', 'Doe', '1234567890', true, 1),
       (2, 'profile2@test', '$2a$10$ukSpo.Ryms/MOgxnjOxriepSjyeM1e0L/SG7A4KQojI0.QesvRc9K', 'b2c7acce-7abb-49ef-91d7-556e0e19c21e', 'profile2_secondary@test',
        'Matt', 'Damon', '222333333', true, 1),
       (3, 'profile3@test', '$2a$10$ukSpo.Ryms/MOgxnjOxriepSjyeM1e0L/SG7A4KQojI0.QesvRc9K', 'b2c7acce-7abb-49ef-91d7-556e0e19c22e', 'profile3_secondary@test',
        'Luke', 'Skywalker', '234234234', true, 2);
ALTER SEQUENCE profile_id_seq RESTART WITH 1000;