insert into organization(id, uuid, name, description)
VALUES (0, 'b2c7acce-7abb-49ef-91d7-556e0e19c15c', 'Default Organization', 'Default Organization Description');
ALTER SEQUENCE organization_id_seq RESTART WITH 10;

insert into profile(id, email, password, uuid, secondary_email, first_name, last_name, phone, enabled, organization_id)
VALUES (0, 'admin@default', '$2a$10$ukSpo.Ryms/MOgxnjOxriepSjyeM1e0L/SG7A4KQojI0.QesvRc9K',
        'b2c7acce-7abb-49ef-91d7-556e0e19c15a', 'admin_secondary@default_secondary',
        'Admin', 'Default', '1234567890', true, 0);
ALTER SEQUENCE profile_id_seq RESTART WITH 10;