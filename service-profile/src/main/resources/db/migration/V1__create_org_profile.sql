-- this must be executed at superuser level
-- CREATE EXTENSION pg_trgm;

create table organization
(
    id                   bigserial not null primary key,
    uuid                 varchar(50) unique not null,
    name                 varchar(255),
    category_description text
);
create index idx_trgm_organization_name on organization(name varchar_pattern_ops);

create table profile
(
    id       bigserial not null primary key,
    email    varchar(100) not null,
    password varchar(100),
    uuid     varchar(50) unique not null,
    enabled  boolean default false,
    secondary_email varchar(100),
    first_name varchar(255),
    last_name varchar(255),
    phone varchar(255),
    organization_id bigint references organization(id)
);
create index idx_trgm_profile_email on profile(email varchar_pattern_ops);
create index idx_profile_email_fn_ln on profile(email varchar_pattern_ops, first_name varchar_pattern_ops, last_name varchar_pattern_ops);
create index idx_profile_secondary_email on profile(secondary_email varchar_pattern_ops);
create index idx_profile_phone on profile(phone varchar_pattern_ops);