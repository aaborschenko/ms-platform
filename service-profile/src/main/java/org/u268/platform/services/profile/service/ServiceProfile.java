package org.u268.platform.services.profile.service;

import org.springframework.stereotype.Service;
import org.u268.platform.services.core.service.AbstractService;
import org.u268.platform.services.profile.dao.entity.Profile;
import org.u268.platform.services.profile.dao.repository.ProfileRepository;
import org.u268.platform.services.profile.dto.ProfileCreateRequestDTO;
import org.u268.platform.services.profile.dto.ProfileResponseDTO;
import org.u268.platform.services.profile.dto.ProfileUpdateRequestDTO;

/**
 * Service for managing {@link Profile}
 *
 * @author Alexey Borschenko
 * @since 05-Sep-2020
 */
@Service
public class ServiceProfile extends
        AbstractService<Profile, ProfileCreateRequestDTO, ProfileUpdateRequestDTO, ProfileResponseDTO> {

    private final OrganizationService organizationService;

    public ServiceProfile(ProfileRepository baseRepository, ProfileMapper baseMapper, OrganizationService organizationService) {
        super(Profile.class, baseRepository, baseMapper);
        this.organizationService = organizationService;
    }

    @Override
    protected void onCreateBeforeSave(ProfileCreateRequestDTO createRequestDto, Profile entityAfterMapping) {

        //setting Organization for new Profile
        entityAfterMapping.setOrganization(organizationService.ensure(createRequestDto.getOrganizationUuid()));
    }

    @Override
    protected void onUpdateBeforeSave(ProfileUpdateRequestDTO updatedRequestDto, Profile entityAfterMapping) {
        //nothing to do here
    }

    @Override
    protected void onDeleteBeforeDeleteFromRepo(Profile entityToDelete) {
        //nothing to do here
    }
}
