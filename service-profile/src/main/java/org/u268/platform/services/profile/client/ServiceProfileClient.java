package org.u268.platform.services.profile.client;

import org.u268.platform.services.profile.dto.AuthResponseDTO;

/**
 * Client interface
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
public interface ServiceProfileClient {

    /**
     * Authenticates user
     *
     * @param organizationUuid organization uuid
     * @param email            email
     * @param password         password
     * @return
     */
    AuthResponseDTO login(String organizationUuid, String email, String password);

    /**
     * Reads current auth user info
     *
     * @return
     */
    AuthResponseDTO getLoginInfo();
}
