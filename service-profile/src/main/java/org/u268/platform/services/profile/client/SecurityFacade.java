package org.u268.platform.services.profile.client;

import org.springframework.security.core.Authentication;

/**
 * Core Facade for Spring Security layer with custom components
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2019
 */
public interface SecurityFacade {

    /**
     * Reads authentication object
     *
     * @return
     */
    Authentication getAuthentication();

    /**
     * Returns current JWT
     *
     * @return
     */
    String getJWT();
}
