package org.u268.platform.services.profile.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.u268.platform.services.core.security.UserID;
import org.u268.platform.services.core.security.UserProfileDetails;
import org.u268.platform.services.profile.dao.entity.Profile;
import org.u268.platform.services.profile.dao.repository.ProfileRepository;

/**
 * Provides User details
 *
 * @author Alexey Borschenko
 * @since 2-Sep-2020
 */
@Primary
@Component
@Slf4j
public class ProfileDetailsService implements UserDetailsService {

    private final ProfileRepository profileRepository;

    public ProfileDetailsService(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserID userID = UserID.fromUsername(username);
        final Profile profile = profileRepository.findByEmailAndOrganizationUuid(userID.getEmail(), userID.getOrganization())
                .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));
        log.trace("ProfileDetailsService.loadUserByUsername: " + profile);
        return new UserProfileDetails(profile);
    }
}