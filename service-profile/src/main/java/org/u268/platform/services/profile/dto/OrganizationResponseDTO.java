package org.u268.platform.services.profile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

/**
 * Organization DTO. Used for reading Organization data
 *
 * @author Alexey Borschenko
 * @since 21-Aug-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Organization DTO. Used for reading Organization data")
public class OrganizationResponseDTO {

    /**
     * External entity ID
     */
    @ApiModelProperty(notes = "External enitity ID")
    private String uuid;

    @ApiModelProperty(notes = "Organization name")
    private String name;

    @ApiModelProperty(notes = "Organization description")
    private String description;
}
