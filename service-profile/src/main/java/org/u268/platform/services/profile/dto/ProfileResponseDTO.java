package org.u268.platform.services.profile.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO with {@link {@link org.u268.platform.services.profile.dao.entity.Profile}} data
 *
 * @author Alexey Borschenko
 * @since 05-Sep-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Profile DTO. Used for reading Profile data")
public class ProfileResponseDTO {

    private String uuid;

    /**
     * There can be many accounts with same email but different organisations
     */
    private String email;

    /**
     * Profile can be turned on / off
     */
    private Boolean enabled;

    /**
     * Secondary email
     */
    private String secondaryEmail;

    private String firstName;

    private String lastName;

    private String phone;

    private String organizationUuid;
}
