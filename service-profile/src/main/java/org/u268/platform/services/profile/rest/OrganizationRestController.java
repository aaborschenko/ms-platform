package org.u268.platform.services.profile.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.u268.platform.services.profile.dao.spec.OrganizationSpecification;
import org.u268.platform.services.profile.dto.OrganizationCreateRequestDTO;
import org.u268.platform.services.profile.dto.OrganizationResponseDTO;
import org.u268.platform.services.profile.dto.OrganizationUpdateRequestDTO;
import org.u268.platform.services.profile.service.OrganizationService;

import javax.validation.Valid;

/**
 * REST service for managing {@link org.u268.platform.services.profile.dao.entity.Organization}
 *
 * @author Alexey Borschenko
 * @since 13-Aug-2020
 */
@Slf4j
@RestController
@RequestMapping("/organizations")
@Api(value = "Organization Rest Controller")
public class OrganizationRestController {

    private final OrganizationService organizationService;

    public OrganizationRestController(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    /**
     * Creates new {@link org.u268.platform.services.profile.dao.entity.Organization}
     *
     * @param dto data
     * @return
     */
    @ApiOperation(value = "Creates new Organization", response = OrganizationResponseDTO.class)
    @PostMapping(path = "/v1", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OrganizationResponseDTO> create(@Valid @RequestBody final OrganizationCreateRequestDTO dto) {
        return ResponseEntity.ok(organizationService.create(dto));
    }

    /**
     * Reads Organization data
     *
     * @param uuid external ID
     * @return
     */
    @ApiOperation(value = "Reads Organization data", response = OrganizationResponseDTO.class)
    @GetMapping(path = "/v1/{uuid}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OrganizationResponseDTO> read(@PathVariable String uuid) {
        return ResponseEntity.ok(organizationService.read(uuid));
    }

    /**
     * Updates Organisation data
     *
     * @param dto data
     * @return
     */
    @ApiOperation(value = "Updates Organization data", response = OrganizationResponseDTO.class)
    @PutMapping(path = "/v1", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OrganizationResponseDTO> update(@Valid @RequestBody final OrganizationUpdateRequestDTO dto) {
        return ResponseEntity.ok(organizationService.update(dto));
    }

    /**
     * Deletes Organization
     *
     * @param uuid external ID
     * @return
     */
    @ApiOperation(value = "Deletes Organization. Responds with deleted entity UUID", response = String.class)
    @DeleteMapping(path = "/v1/{uuid}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> delete(@PathVariable String uuid) {
        return ResponseEntity.ok(organizationService.delete(uuid));
    }

    /**
     * Searches Organizations
     *
     * @param pageable page data
     * @param nameStart organization name start
     * @return
     */
    @ApiOperation(value = "Reads Organization data", response = OrganizationResponseDTO.class, responseContainer = "Page")
    @GetMapping(path = "/v1", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Page<OrganizationResponseDTO>> search(
            Pageable pageable,
            @RequestParam(required = false) String nameStart) {
        return ResponseEntity.ok(organizationService.search(new OrganizationSpecification(nameStart), pageable));
    }
}
