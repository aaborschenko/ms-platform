package org.u268.platform.services.profile.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.u268.platform.services.core.security.UserID;
import org.u268.platform.services.core.security.UsernamePasswordAuthenticationTokenJWT;
import org.u268.platform.services.profile.dto.AuthResponseDTO;

import static java.util.stream.Collectors.toList;

/**
 * Custom authentication provider to use with <b>service-profile</b> REST Auth endpoint
 * <p>
 * This component should be used by other Micro Services and Applications who wants to authenticate with ServiceProfile MS
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@Slf4j
public class RemoteAuthenticationProvider implements AuthenticationProvider {

    private final ServiceProfileClient serviceProfileClient;

    public RemoteAuthenticationProvider(ServiceProfileClient serviceProfileClient) {
        this.serviceProfileClient = serviceProfileClient;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        log.debug("RemoteAuthenticationProvider: authenticating {}", authentication.getName());

        //Getting Organization UUID and Email from Authentication.name value
        final UserID userID = UserID.fromUsername(authentication.getName());

        //authenticating
        final AuthResponseDTO authResponse = serviceProfileClient.login(
                userID.getOrganization(), userID.getEmail(), authentication.getCredentials().toString());

        //constructing response using original username in form of organization/email
        return new UsernamePasswordAuthenticationTokenJWT(
                authentication.getName(),
                null,
                authResponse.getRoles().stream().map(SimpleGrantedAuthority::new).collect(toList()),
                authResponse.getToken()
        );
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
