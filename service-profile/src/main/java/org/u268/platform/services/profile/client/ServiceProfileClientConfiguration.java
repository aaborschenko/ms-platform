package org.u268.platform.services.profile.client;

import org.springframework.context.annotation.Configuration;
import org.u268.platform.services.core.client.BaseClientConfig;

/**
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@Configuration
public class ServiceProfileClientConfiguration extends BaseClientConfig {
}
