package org.u268.platform.services.profile.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.u268.platform.services.profile.dto.AuthRequestDTO;
import org.u268.platform.services.profile.dto.AuthResponseDTO;

/**
 * Interface implementation
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@Component
@Slf4j
public class ServiceProfileClientImpl extends BaseClientWrapperWithSecurity implements ServiceProfileClient {

    private final ServiceProfileFeignClient feignClient;

    protected ServiceProfileClientImpl(SecurityFacade securityFacade, ServiceProfileFeignClient feignClient) {
        super(securityFacade);
        this.feignClient = feignClient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthResponseDTO login(String organizationUuid, String email, String password) {
        return feignClient.login(new AuthRequestDTO(organizationUuid, email, password)).getBody();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthResponseDTO getLoginInfo() {
        return feignClient.info(getJwtHeaderValue()).getBody();
    }
}
