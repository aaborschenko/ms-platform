package org.u268.platform.services.profile.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.u268.platform.services.profile.client.SecurityFacade;
import org.u268.platform.services.core.security.UserID;
import org.u268.platform.services.profile.dao.entity.Profile;
import org.u268.platform.services.profile.dto.AuthRequestDTO;
import org.u268.platform.services.profile.dto.AuthResponseDTO;
import org.u268.platform.services.profile.service.AuthService;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.ok;

/**
 * REST controller for authentication purposes
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2019
 */
@Slf4j
@RestController
@RequestMapping("/auth")
@Api(value = "Authentication Controller")
public class AuthRestController {

    private final AuthService authService;
    private final SecurityFacade securityFacade;

    public AuthRestController(AuthService authService, SecurityFacade securityFacade) {
        this.authService = authService;
        this.securityFacade = securityFacade;
    }

    /**
     * Auth endpoint
     *
     * @param authRequestDTO
     * @return
     */
    @ApiOperation(value = "User Auth", response = AuthResponseDTO.class)
    @PostMapping("/v1")
    public ResponseEntity<AuthResponseDTO> login(@RequestBody AuthRequestDTO authRequestDTO) {
        log.info("Got login request: organization={}, email={}", authRequestDTO.getOrganizationUuid(), authRequestDTO.getEmail());
        try {
            final UserID userID = new UserID(authRequestDTO.getOrganizationUuid(), authRequestDTO.getEmail());
            final String token = authService.authenticate(userID, authRequestDTO.getPassword());
            final Profile authenticatedProfile = authService.findProfile(userID);
            final AuthResponseDTO response = new AuthResponseDTO(
                    authRequestDTO.getOrganizationUuid(),
                    authRequestDTO.getEmail(),
                    authenticatedProfile.getUuid(),
                    token,
                    authenticatedProfile.getRoles());

            return ok(response);
        } catch (AuthenticationException e) {
            log.error("Unable to auth user: " + authRequestDTO.getEmail() + ". Error: " + e.getMessage(), e);
            throw new BadCredentialsException("Invalid username/password provided", e);
        }
    }

    /**
     * Reads current user info based on provided Auth token
     *
     * @return
     */
    @ApiOperation(value = "Reads user info from provided auth token", response = AuthResponseDTO.class)
    @ApiImplicitParam(name = "Authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "Bearer access_token")
    @GetMapping("/v1")
    public ResponseEntity<AuthResponseDTO> info() {

        final UserID userID = UserID.fromUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        final Profile profile = authService.findProfile(userID);

        final AuthResponseDTO responseDTO = new AuthResponseDTO(
                profile.getOrganization().getUuid(),
                profile.getEmail(),
                profile.getUuid(),
                securityFacade.getJWT(),
                SecurityContextHolder.getContext().getAuthentication().getAuthorities()
                        .stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(toList())
        );

        log.debug("Done reading login info: " + responseDTO);
        return ok(responseDTO);
    }
}
