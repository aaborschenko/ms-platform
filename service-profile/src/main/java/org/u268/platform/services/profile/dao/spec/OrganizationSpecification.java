package org.u268.platform.services.profile.dao.spec;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.domain.Specification;
import org.u268.platform.services.profile.dao.entity.Organization;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Spec interface for searching by name start
 *
 * @author Alexey Borschenko
 * @since 14-Sep-2020
 */
@AllArgsConstructor
public class OrganizationSpecification implements Specification<Organization> {

    private final String nameStart;

    @Override
    public Predicate toPredicate(
            @NotNull Root<Organization> root,
            @NotNull CriteriaQuery<?> criteriaQuery,
            @NotNull CriteriaBuilder criteriaBuilder) {

        final List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.isNotEmpty(nameStart)) {
            Path<String> name = root.get("name");
            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(name), nameStart.toLowerCase() + "%"));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
