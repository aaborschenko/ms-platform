package org.u268.platform.services.profile.client;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.u268.platform.services.core.security.UsernamePasswordAuthenticationTokenJWT;

/**
 * Default implementation
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@Component
public class SecurityFacadeImpl implements SecurityFacade {
    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public String getJWT() {
        final UsernamePasswordAuthenticationTokenJWT authenticationTokenJWT =
                (UsernamePasswordAuthenticationTokenJWT) SecurityContextHolder.getContext().getAuthentication();
        return authenticationTokenJWT == null ? null : authenticationTokenJWT.getJwt();
    }
}
