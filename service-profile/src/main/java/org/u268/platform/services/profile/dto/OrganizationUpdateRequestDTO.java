package org.u268.platform.services.profile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.u268.platform.services.core.dto.UuidDto;

import javax.validation.constraints.NotEmpty;

/**
 * DTO used to update {@link org.u268.platform.services.profile.dao.entity.Organization}
 *
 * @author Alexey Borschenko
 * @since 21-Aug-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "DTO used to update Organization")
public class OrganizationUpdateRequestDTO implements UuidDto {

    @NotEmpty(message = "uuid required")
    @ApiModelProperty(notes = "External entity ID")
    private String uuid;

    @NotEmpty(message = "Name required")
    @ApiModelProperty(notes = "Name")
    private String name;

    @ApiModelProperty(notes = "Organization description")
    private String description;
}
