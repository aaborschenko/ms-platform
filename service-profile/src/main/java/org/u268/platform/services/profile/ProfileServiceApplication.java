package org.u268.platform.services.profile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring boot bootstrap application class
 *
 * @author Alexey Borschenko
 * @since 13-Aug-2020
 */
@SpringBootApplication(scanBasePackages = "org.u268.platform.services")
@EntityScan
@EnableJpaRepositories
@EnableFeignClients
public class ProfileServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProfileServiceApplication.class, args);
    }

}
