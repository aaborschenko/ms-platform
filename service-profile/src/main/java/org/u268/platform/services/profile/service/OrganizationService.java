package org.u268.platform.services.profile.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.u268.platform.services.core.service.AbstractService;
import org.u268.platform.services.profile.dao.entity.Organization;
import org.u268.platform.services.profile.dao.repository.OrganizationRepository;
import org.u268.platform.services.profile.dao.spec.OrganizationSpecification;
import org.u268.platform.services.profile.dto.OrganizationCreateRequestDTO;
import org.u268.platform.services.profile.dto.OrganizationResponseDTO;
import org.u268.platform.services.profile.dto.OrganizationUpdateRequestDTO;

/**
 * Service for managing {@link Organization}
 *
 * @author Alexey Borschenko
 * @since 21-Aug-2020
 */
@Service
public class OrganizationService extends
        AbstractService<Organization, OrganizationCreateRequestDTO, OrganizationUpdateRequestDTO, OrganizationResponseDTO> {

    public OrganizationService(OrganizationRepository baseRepository, OrganizationMapper baseMapper) {
        super(Organization.class, baseRepository, baseMapper);
    }

    @Override
    protected void onCreateBeforeSave(OrganizationCreateRequestDTO createRequestDto, Organization entityAfterMapping) {
        //nothing to do here
    }

    @Override
    protected void onUpdateBeforeSave(OrganizationUpdateRequestDTO updatedRequestDto, Organization entityAfterMapping) {
        //nothing to do here
    }

    @Override
    protected void onDeleteBeforeDeleteFromRepo(Organization entityToDelete) {
        //nothing to do here
    }
}
