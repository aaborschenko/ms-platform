package org.u268.platform.services.profile.dao.repository;

import org.u268.platform.services.core.dao.repository.IBaseRepository;
import org.u268.platform.services.profile.dao.entity.Organization;

import java.util.Optional;

/**
 * JPA Repository for {@link Organization}
 *
 * @author Alexey Borschenko
 * @since 16-Aug-2020
 */
public interface OrganizationRepository extends IBaseRepository<Organization> {

    Optional<Organization> findByUuid(String uuid);
}
