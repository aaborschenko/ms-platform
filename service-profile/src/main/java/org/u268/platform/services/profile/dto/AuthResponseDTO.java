package org.u268.platform.services.profile.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Authentication response
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2019
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthResponseDTO implements Serializable {

    private String organizationUuid;
    private String email;
    private String uuid;
    private String token;
    private List<String> roles;

    public AuthResponseDTO(String organizationUuid, String email, String uuid, String token) {
        this.organizationUuid = organizationUuid;
        this.email = email;
        this.uuid = uuid;
        this.token = token;
        this.roles = Collections.singletonList("USER");
    }
}
