package org.u268.platform.services.profile.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.u268.platform.services.core.configuration.SecurityConfig;

/**
 * @author Alexey Borschenko
 * @since 03-Sep-2020
 */
@EnableWebSecurity
public class ApplicationSecurityConfig extends SecurityConfig {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and()
                .httpBasic().disable()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()

                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/organizations/v1").permitAll()
                .antMatchers(HttpMethod.PUT, "/organizations/v1").permitAll()
                .antMatchers(HttpMethod.GET, "/organizations/v1").permitAll()
                .antMatchers(HttpMethod.DELETE, "/organizations/v1").permitAll()

                //
                .antMatchers(HttpMethod.GET, "/utils/version/v1").permitAll()

                .and()
                .apply(jwtConfigurer);
    }
}
