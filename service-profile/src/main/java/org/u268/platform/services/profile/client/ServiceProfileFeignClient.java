package org.u268.platform.services.profile.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.u268.platform.services.profile.dto.AuthRequestDTO;
import org.u268.platform.services.profile.dto.AuthResponseDTO;

/**
 * Client for accessing service endpoints
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@FeignClient(name = "service-profile")
public interface ServiceProfileFeignClient {

    /**
     * Sign In
     *
     * @param authRequestDTO
     * @return
     */
    @PostMapping("/auth/v1")
    ResponseEntity<AuthResponseDTO> login(@RequestBody AuthRequestDTO authRequestDTO);

    /**
     * Reads authenticated user info
     *
     * @return
     */
    @GetMapping("/auth/v1")
    ResponseEntity<AuthResponseDTO> info(@RequestHeader("Authorization") String authToken);
}
