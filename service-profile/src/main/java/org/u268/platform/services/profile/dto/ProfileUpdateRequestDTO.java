package org.u268.platform.services.profile.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.u268.platform.services.core.dto.UuidDto;

/**
 * DTO used to update {@link org.u268.platform.services.profile.dao.entity.Profile}
 *
 * @author Alexey Borschenko
 * @since 05-Sep-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "DTO used to update Profile")
public class ProfileUpdateRequestDTO implements UuidDto {

    private String uuid;

    /**
     * Secondary email
     */
    private String secondaryEmail;

    private String firstName;

    private String lastName;

    private String phone;
}
