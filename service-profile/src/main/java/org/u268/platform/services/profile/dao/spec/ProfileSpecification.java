package org.u268.platform.services.profile.dao.spec;

import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.domain.Specification;
import org.u268.platform.services.profile.dao.entity.Profile;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * Spec interface for searching Profiles
 *
 * @author Alexey Borschenko
 * @since 15-Sep-2020
 */
@AllArgsConstructor
public class ProfileSpecification implements Specification<Profile> {

    private final String emailStart;
    private final String firstNameStart;
    private final String lastNameStart;
    private final String phoneStart;
    private final String organizationUuid;

    @Override
    public Predicate toPredicate(
            @NotNull Root<Profile> root,
            @NotNull CriteriaQuery<?> criteriaQuery,
            @NotNull CriteriaBuilder criteriaBuilder) {

        final List<Predicate> predicates = new ArrayList<>();

        if (StringUtils.isNotEmpty(emailStart)) {
            predicates.add(criteriaBuilder
                    .like(criteriaBuilder.lower(root.get("email")), emailStart.toLowerCase() + "%"));
        }
        if (StringUtils.isNotEmpty(firstNameStart)) {
            predicates.add(criteriaBuilder
                    .like(criteriaBuilder.lower(root.get("firstName")), firstNameStart.toLowerCase() + "%"));
        }
        if (StringUtils.isNotEmpty(lastNameStart)) {
            predicates.add(criteriaBuilder
                    .like(criteriaBuilder.lower(root.get("lastName")), lastNameStart.toLowerCase() + "%"));
        }
        if (StringUtils.isNotEmpty(phoneStart)) {
            predicates.add(criteriaBuilder
                    .like(criteriaBuilder.lower(root.get("phone")), phoneStart.toLowerCase() + "%"));
        }
        if (StringUtils.isNotEmpty(organizationUuid)) {
            predicates.add(criteriaBuilder
                    .equal(criteriaBuilder.lower(root.get("organization").get("uuid")), organizationUuid.toLowerCase()));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
