package org.u268.platform.services.profile.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

/**
 * DTO used to create {@link org.u268.platform.services.profile.dao.entity.Profile}
 *
 * @author Alexey Borschenko
 * @since 05-Sep-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "DTO used to create Profile")
public class ProfileCreateRequestDTO {

    /**
     * There can be many accounts with same email but different organisations
     */
    private String email;

    /**
     * Profile can be turned on / off
     */
    private Boolean enabled;

    /**
     * Profile password
     */
    private String password;

    /**
     * Secondary email
     */
    private String secondaryEmail;

    private String firstName;

    private String lastName;

    private String phone;

    private String organizationUuid;
}
