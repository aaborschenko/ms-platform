package org.u268.platform.services.profile.service;

import org.u268.platform.services.core.security.UserID;
import org.u268.platform.services.profile.dao.entity.Profile;

/**
 * Authentication service backend
 *
 * @author Alexey Borschenko
 * @since 12-Feb-2020
 */
public interface AuthService {

    String authenticate(UserID userID, String password);

    String preAuthenticate(UserID userID);

    Profile findProfile(UserID userID);
}
