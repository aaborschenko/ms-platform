package org.u268.platform.services.profile.dao.repository;

import org.u268.platform.services.core.dao.repository.IBaseRepository;
import org.u268.platform.services.profile.dao.entity.Profile;

import java.util.Optional;

/**
 * JPA Repository for {@link Profile}
 *
 * @author Alexey Borschenko
 * @since 3-Sep-2020
 */
public interface ProfileRepository extends IBaseRepository<Profile> {

    /**
     * Searches for profile by email and Organization uuid
     *
     * @param email
     * @param organizationUuid
     * @return
     */
    Optional<Profile> findByEmailAndOrganizationUuid(String email, String organizationUuid);
}
