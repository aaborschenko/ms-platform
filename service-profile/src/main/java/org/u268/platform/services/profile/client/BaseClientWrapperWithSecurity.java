package org.u268.platform.services.profile.client;

/**
 * Base class for all Micro Services Client wrappers. It requires {@link SecurityFacade} implementation to be injected.
 * Then it will provide wrapper method {@link #getJwtHeaderValue()} which constructs JWT (Auth) header value for client requests.
 * In your wrapper for Feign client calls you will use it like:
 * <code>
 * public void deleteMedia(String key) {
 * filesMSFeignClient.deleteMedia(getJwtHeaderValue(), key);
 * }
 * </code>
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
public abstract class BaseClientWrapperWithSecurity {

    protected final SecurityFacade securityFacade;

    protected BaseClientWrapperWithSecurity(SecurityFacade securityFacade) {
        this.securityFacade = securityFacade;
    }

    protected String getJwtHeaderValue() {
        return "Bearer " + securityFacade.getJWT();
    }
}
