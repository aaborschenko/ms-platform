package org.u268.platform.services.profile.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Authentication request DTO
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@ApiModel(description = "Authentication request DTO")
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class AuthRequestDTO {

    private String organizationUuid;
    private String email;
    private String password;

}
