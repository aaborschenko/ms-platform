package org.u268.platform.services.profile.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

/**
 * DTO for creation of {@link org.u268.platform.services.profile.dao.entity.Organization}
 *
 * @author Alexey Borschenko
 * @since 21-Aug-2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Organization Create Request DTO. Use it to create Organization")
public class OrganizationCreateRequestDTO {

    @NotEmpty(message = "Name required")
    @ApiModelProperty(notes = "Name")
    private String name;

    @ApiModelProperty(notes = "Organization description")
    private String description;
}
