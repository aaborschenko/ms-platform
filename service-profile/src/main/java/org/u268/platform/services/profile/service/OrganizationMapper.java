package org.u268.platform.services.profile.service;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.u268.platform.services.core.configuration.BaseMapperConfig;
import org.u268.platform.services.core.service.BaseMapper;
import org.u268.platform.services.profile.dao.entity.Organization;
import org.u268.platform.services.profile.dto.OrganizationCreateRequestDTO;
import org.u268.platform.services.profile.dto.OrganizationResponseDTO;
import org.u268.platform.services.profile.dto.OrganizationUpdateRequestDTO;

/**
 * Data mapper for {@link org.u268.platform.services.profile.dao.entity.Organization} and its DTOs
 * <p>
 * Because MapStruct is NOT working with generics - we need exact class names here in overridden methods
 *
 * @author Alexey Borschenko
 * @since 21-Aug-2020
 */
@Mapper(config = BaseMapperConfig.class)
public interface OrganizationMapper extends
        BaseMapper<Organization, OrganizationCreateRequestDTO, OrganizationUpdateRequestDTO, OrganizationResponseDTO> {

    @Override
    public OrganizationResponseDTO entityToDto(Organization entity);

    @Override
    public Organization newEntity(OrganizationCreateRequestDTO dto);

    @Override
    public Organization updateEntity(OrganizationUpdateRequestDTO dto, @MappingTarget Organization entityToUpdate);
}
