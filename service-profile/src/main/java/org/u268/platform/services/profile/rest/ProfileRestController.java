package org.u268.platform.services.profile.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.u268.platform.services.profile.dao.entity.Profile;
import org.u268.platform.services.profile.dao.spec.ProfileSpecification;
import org.u268.platform.services.profile.dto.ProfileCreateRequestDTO;
import org.u268.platform.services.profile.dto.ProfileResponseDTO;
import org.u268.platform.services.profile.dto.ProfileUpdateRequestDTO;
import org.u268.platform.services.profile.service.ServiceProfile;

import javax.validation.Valid;

/**
 * REST service for managing {@link Profile}
 *
 * @author Alexey Borschenko
 * @since 13-Aug-2020
 */
@Slf4j
@RestController
@RequestMapping("/profiles")
@Api(value = "Profile Rest Controller")
public class ProfileRestController {

    private final ServiceProfile profileService;

    public ProfileRestController(ServiceProfile profileService) {
        this.profileService = profileService;
    }

    /**
     * Creates new {@link org.u268.platform.services.profile.dao.entity.Profile}
     *
     * @param dto data
     * @return
     */
    @ApiOperation(value = "Creates new Profile", response = ProfileResponseDTO.class)
    @PostMapping(path = "/v1", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ProfileResponseDTO> create(@Valid @RequestBody final ProfileCreateRequestDTO dto) {
        return ResponseEntity.ok(profileService.create(dto));
    }

    /**
     * Reads Profile data
     *
     * @param uuid external ID
     * @return
     */
    @ApiOperation(value = "Reads Profile data", response = ProfileResponseDTO.class)
    @GetMapping(path = "/v1/{uuid}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ProfileResponseDTO> read(@PathVariable String uuid) {
        return ResponseEntity.ok(profileService.read(uuid));
    }

    /**
     * Updates Organisation data
     *
     * @param dto data
     * @return
     */
    @ApiOperation(value = "Updates Profile data", response = ProfileResponseDTO.class)
    @PutMapping(path = "/v1", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ProfileResponseDTO> update(@Valid @RequestBody final ProfileUpdateRequestDTO dto) {
        return ResponseEntity.ok(profileService.update(dto));
    }

    /**
     * Deletes Profile
     *
     * @param uuid external ID
     * @return
     */
    @ApiOperation(value = "Deletes Profile. Responds with deleted entity UUID", response = String.class)
    @DeleteMapping(path = "/v1/{uuid}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> delete(@PathVariable String uuid) {
        return ResponseEntity.ok(profileService.delete(uuid));
    }

    /**
     * Searches Profiles
     *
     * @param pageable         page data
     * @param emailStart       email start
     * @param firstNameStart   first name start
     * @param lastNameStart    last name start
     * @param phoneStart       phone start
     * @param organizationUuid Profile's Organization uuid
     * @return page data with profiles
     */
    @ApiOperation(value = "Searches Profile data", response = ProfileResponseDTO.class, responseContainer = "Page")
    @GetMapping(path = "/v1", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Page<ProfileResponseDTO>> search(
            Pageable pageable,
            @RequestParam(required = false) String emailStart,
            @RequestParam(required = false) String firstNameStart,
            @RequestParam(required = false) String lastNameStart,
            @RequestParam(required = false) String phoneStart,
            @RequestParam(required = false) String organizationUuid) {
        return ResponseEntity.ok(profileService.search(
                new ProfileSpecification(emailStart, firstNameStart, lastNameStart, phoneStart, organizationUuid), pageable));
    }
}
