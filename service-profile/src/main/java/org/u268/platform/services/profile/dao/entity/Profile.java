package org.u268.platform.services.profile.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.u268.platform.services.core.security.IProfile;
import org.u268.platform.services.core.security.UserID;
import org.u268.platform.services.core.service.UuidEntity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Collections;
import java.util.List;

/**
 * Profile entity
 *
 * @author Alexey Borschenko
 * @since 13-Aug-2020
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "profile")
public class Profile implements IProfile, UuidEntity {

    /**
     * This Primary Key value SHOULD NOT be shared in ANY external links or DTOs
     */
    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * This should be used as ID in external links
     */
    @Column(name = "uuid", unique = true, nullable = false)
    private String uuid;

    /**
     * There can be many accounts with same email but different organisations
     */
    @Column(name = "email", unique = false, nullable = false)
    private String email;

    /**
     * Profile can be turned on / off
     */
    @Column(name = "enabled")
    private Boolean enabled;

    /**
     * Profile password
     */
    @Column(name = "password")
    private String password;

    /**
     * Secondary email
     */
    @Column(name = "secondary_email", unique = false)
    private String secondaryEmail;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Organization organization;

    @Override
    public UserID getUserID() {
        return new UserID(organization != null ? organization.getUuid() : "NOT_AVAILABLE", email);
    }

    @Override
    public List<String> getRoles() {
        return Collections.singletonList("ROLE_USER");
    }
}
