package org.u268.platform.services.profile.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.u268.platform.services.core.security.JwtTokenProvider;
import org.u268.platform.services.core.security.UserID;
import org.u268.platform.services.profile.dao.entity.Profile;
import org.u268.platform.services.profile.dao.repository.ProfileRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Auth service implementation
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
@Service
@Slf4j
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final ProfileRepository profileRepository;

    public AuthServiceImpl(
            final AuthenticationManager authenticationManager,
            final JwtTokenProvider jwtTokenProvider,
            final PasswordEncoder passwordEncoder, ProfileRepository profileRepository) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.profileRepository = profileRepository;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String authenticate(final UserID userID, final String password) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userID.toUsername(), password));

        final Profile profile = findProfile(userID);

        return jwtTokenProvider.createToken(userID.toUsername(), profile.getRoles());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String preAuthenticate(final UserID userID) {

        final Profile profile = findProfile(userID);

        final List<SimpleGrantedAuthority> grantedAuthorities =
                profile.getRoles()
                        .stream()
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());

        final PreAuthenticatedAuthenticationToken authentication =
                new PreAuthenticatedAuthenticationToken(userID.toUsername(), null, grantedAuthorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return jwtTokenProvider.createToken(userID.toUsername(), profile.getRoles());
    }

    /**
     * {@inheritDoc}
     */
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public Profile findProfile(UserID userID) {
        return profileRepository
                .findByEmailAndOrganizationUuid(userID.getEmail(), userID.getOrganization())
                .orElseThrow(() -> new UsernameNotFoundException("User [" + userID + "] not found"));
    }
}
