package org.u268.platform.services.profile.service;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.u268.platform.services.core.configuration.BaseMapperConfig;
import org.u268.platform.services.core.service.BaseMapper;
import org.u268.platform.services.profile.dao.entity.Profile;
import org.u268.platform.services.profile.dto.ProfileCreateRequestDTO;
import org.u268.platform.services.profile.dto.ProfileResponseDTO;
import org.u268.platform.services.profile.dto.ProfileUpdateRequestDTO;

/**
 * Data mapper for {@link Profile} and its DTOs
 *
 * @author Alexey Borschenko
 * @since 5-Sep-2020
 */
@Mapper(config = BaseMapperConfig.class)
public interface ProfileMapper extends
        BaseMapper<Profile, ProfileCreateRequestDTO, ProfileUpdateRequestDTO, ProfileResponseDTO> {

    @Mapping(target = "organizationUuid", source = "organization.uuid")
    @Override
    ProfileResponseDTO entityToDto(Profile entity);

    @Override
    Profile newEntity(ProfileCreateRequestDTO dto);

    @Override
    Profile updateEntity(ProfileUpdateRequestDTO dto, @MappingTarget Profile entityToUpdate);
}
