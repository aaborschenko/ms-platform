package org.u268.platform.ui.admin.security;

/**
 * User is NOT Admin
 *
 * @author Alexey Borschenko
 * @since 19-Sep-2020
 */
public class NotAdminProfile extends RuntimeException {
    public NotAdminProfile() {
        super();
    }

    public NotAdminProfile(String message) {
        super(message);
    }

    public NotAdminProfile(String message, Throwable cause) {
        super(message, cause);
    }

    public NotAdminProfile(Throwable cause) {
        super(cause);
    }

    protected NotAdminProfile(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
