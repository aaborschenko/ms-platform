package org.u268.platform.ui.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.vaadin.artur.helpers.LaunchUtil;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication(
        scanBasePackages = {
                "org.u268.platform.ui.admin",
//                "org.u268.platform.services.core",
                "org.u268.platform.services.profile.client"
        }
)
@EnableFeignClients(
        basePackages = {
                "org.u268.platform.services.profile.client"
        }
)
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        LaunchUtil.launchBrowserInDevelopmentMode(SpringApplication.run(Application.class, args));
    }

}
