package org.u268.platform.ui.admin.data.service;

import org.u268.platform.ui.admin.data.entity.Person;

import org.springframework.data.jpa.repository.JpaRepository;
import java.time.LocalDate;

public interface PersonRepository extends JpaRepository<Person, Integer> {

}